package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"text/template"

	"gitlab.com/watud/genstructgraphql/schema"
)

// Tmpl structure of the graphql-go
type Tmpl struct {
	Package   string
	Imports   []string
	Queries   []Schema
	Mutations []Schema
	Types     []Types
	Enums     []schema.Enum
	Inputs    []Inputs
	Scalars   []Scalar
}

var baseDir string
var nameFile = "graphql.go"

var (
	flagSchema   = flag.String("schema", "", `Nombre del archivo que contiene el esquema. Ejemplo -schema="modulo.graphql"`)
	flagNameFile = flag.String("file-name", nameFile, "Nombre del archivo para guardar la estructura")
)

func main() {
	flag.Parse()

	if !strings.Contains(*flagSchema, ".graphql") {
		fmt.Fprintf(os.Stderr, "-schema=... is requerido.\n")
		os.Exit(1)
	}

	var name string
	baseDir, _ = filepath.Abs(filepath.Dir(*flagSchema))
	_, name = path.Split(*flagSchema)
	baseDir += "/"

	if _, err := os.Stat(baseDir + name); os.IsNotExist(err) {
		log.Fatalf("No existe el archivo: %s%s", baseDir, name)
	}

	nameFile = *flagNameFile
	readFile(name)
}

func readFile(name string) {
	var s schema.Schema
	bytes, _ := ioutil.ReadFile(baseDir + name)
	s.GetSchema(string(bytes))

	commentNameFile := schema.ParseRegex(`(?s)(?m)^\#[ \t]*\+(.*)\.go[ \t]*$`, string(bytes))
	if len(commentNameFile) > 0 {
		nameFile = fmt.Sprintf("%v.go", commentNameFile[0][0])
	}

	tmpl := &Tmpl{
		Package: strings.Split(name, ".")[0],
		Imports: []string{`"github.com/graphql-go/graphql"`},
		Enums:   s.Enums,
	}
	if len(s.Scalars) > 0 {
		tmpl.Scalars = buildScalar(s.Scalars)
		tmpl.Imports = append(tmpl.Imports, `"github.com/graphql-go/graphql/language/ast"`)
	}
	tmpl.Inputs = buildInputs(s.Inputs)
	tmpl.Queries = buildSchema(s.Query)
	tmpl.Mutations = buildSchema(s.Mutation)
	tmpl.Types = buildTypes(s.Types)

	recoveryScalar(tmpl)
	recoveryResolve(tmpl)
	recoveryImport(tmpl)
	generate(tmpl)
}

func convertTitle(s string) string {
	return fmt.Sprintf("%s%s", strings.ToUpper(s[:1]), s[1:])
}

func generate(data *Tmpl) {
	nameFile := baseDir + nameFile

	f, _ := os.OpenFile(nameFile, os.O_CREATE|os.O_WRONLY, 0644)

	bytes, err := Asset("plantilla.tmpl")
	checkErr(err)
	t, err := template.New("MD").Parse(string(bytes))
	checkErr(err)
	err = t.Execute(f, data)
	checkErr(err)
	fmt.Printf("Se genero el archivo: %s", nameFile)
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
