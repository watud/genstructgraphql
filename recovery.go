package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"gitlab.com/watud/genstructgraphql/schema"
)

var nameFileRecovery = ""

func recovery() (bool, string) {
	if _, err := os.Stat(baseDir + nameFile); !os.IsNotExist(err) {
		b, err := ioutil.ReadFile(baseDir + nameFile)
		checkErr(err)
		nameFileRecovery = fmt.Sprintf("_%s.%d.go", strings.Split(nameFile, ".")[0], time.Now().UnixNano())
		ioutil.WriteFile(baseDir+nameFileRecovery, b, 0644)
		os.Remove(baseDir + nameFile)
		return true, string(b)
	}

	if nameFileRecovery == "" {
		return false, ""
	}

	b, err := ioutil.ReadFile(baseDir + nameFileRecovery)
	checkErr(err)
	return true, string(b)
}

func recoveryResolve(data *Tmpl) {
	ok, file := recovery()
	if !ok {
		return
	}

	resolves := make(map[string]string)
	for _, v := range schema.ParseRegex(`(?s)(?m)"([\w _-]+)":[\t ]*&graphql.Field.*?Resolve:(.*?),[\t\n ]*}`, file) {
		resolves[string(v[0])] = string(v[1]) + ","
	}

	for i, q := range data.Queries {
		if value, ok := resolves[q.Name]; ok {
			data.Queries[i].Resolve = value
		}
	}

	for i, m := range data.Mutations {
		if value, ok := resolves[m.Name]; ok {
			data.Mutations[i].Resolve = value
		}
	}
}

func recoveryScalar(data *Tmpl) {
	ok, file := recovery()
	if !ok {
		return
	}

	s := make(map[string]int)
	for i, v := range data.Scalars {
		s[v.Name] = i
	}

	for _, v := range schema.ParseRegex(`(?s)(?m)type[\t ]*([\w\d_-]*)[\t ]*struct[\t ]*{(.*?)[^{]}\n`, file) {
		if i, ok := s[v[0]]; ok {
			data.Scalars[i].Struct = v[1]

			data.Scalars[i].Func = schema.ParseRegex(fmt.Sprintf(`(?s)(?m)func[\t ]*\(scalar[\t ]*\*%s\)[\t ]*(.*?)^}\n`, data.Scalars[i].Name), file)[0][0] + "}"
			data.Scalars[i].FuncNew = schema.ParseRegex(fmt.Sprintf(`(?s)(?m)func[\t ]*New%s(.*?)^}\n`, data.Scalars[i].Name), file)[0][0] + "}"

			funcs := schema.ParseRegex(fmt.Sprintf(`(?s)(?m)var[\t ]*%sScalarType.*?Serialize:.*?{[^}](.*?)},[\t\n ]*ParseValue:.*?{[^}](.*?)},[\t\n ]*ParseLiteral:.*?{[^}](.*?)},[\t\n ]*`, data.Scalars[i].Name), file)[0]
			data.Scalars[i].FuncSerialize = funcs[0]
			data.Scalars[i].FuncParseValue = funcs[1]
			data.Scalars[i].FuncParseLiteral = funcs[2]
		}
	}
}

func recoveryImport(data *Tmpl) {
	ok, file := recovery()
	if !ok {
		return
	}

	m := make(map[string]bool)
	imports := schema.ParseRegex(`import[\t ]*\(\n?([\n\t\w-\/"._ ]*)\)`, file)[0][0]
	for _, v := range schema.ParseRegex(`[\t ]*"([\n\t\w-\/._ ]*)"[\t ]*`, imports) {
		m[fmt.Sprintf(`"%s"`, v[0])] = true
	}

	for _, v := range data.Imports {
		if _, ok := m[v]; ok {
			delete(m, v)
		}
	}

	for k := range m {
		data.Imports = append(data.Imports, k)
	}
}
