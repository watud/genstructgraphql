module gitlab.com/watud/genstructgraphql

go 1.12

require (
	github.com/graphql-go/graphql v0.7.8
	github.com/vektah/gqlparser v1.1.2
)
