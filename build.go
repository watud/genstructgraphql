package main

import (
	"fmt"
	"strings"

	"gitlab.com/watud/genstructgraphql/schema"
)

// Arg structure for template arguments
type Arg struct {
	Name         string
	Description  string
	Type         string
	DefaultValue interface{}
}

// Schema structure for template schemas
type Schema struct {
	Name        string
	Type        string
	Description string
	Args        []Arg
	Resolve     string
}

// Field structure for template field
type Field struct {
	Name        string
	Description string
	Type        string
	TypeGo      string
}

// Types structure for template types
type Types struct {
	Name        string
	Description string
	Fields      []Field
}

// Inputs structure for template inputs
type Inputs struct {
	Name        string
	Description string
	Fields      []Arg
}

// Scalar structure for template scalars
type Scalar struct {
	Name             string
	Description      string
	Struct           string
	Func             string
	FuncNew          string
	FuncSerialize    string
	FuncParseValue   string
	FuncParseLiteral string
}

func buildSchema(fragments []schema.Fragment) []Schema {
	var schemas []Schema

	for _, v := range fragments {
		qq := Schema{
			Name:        v.Name,
			Description: convertTitle(v.Description),
			Type:        buildType(v.Result, v.Type.IsNotNull, v.Type.IsArray, v.Type.IsNotNullArray),
			Resolve:     `func(p graphql.ResolveParams) (interface{}, error) { return nil, nil },`,
		}

		if len(v.Args) > 0 {
			qq.Args = make([]Arg, len(v.Args))
			for i, a := range v.Args {
				qq.Args[i].Name = a.Name
				qq.Args[i].Type = buildType(a.Value, a.Type.IsNotNull, a.Type.IsArray, a.Type.IsNotNullArray)
				if a.DefaultValue != nil {
					switch fmt.Sprintf("%T", a.DefaultValue) {
					case "string":
						qq.Args[i].DefaultValue = fmt.Sprintf(`"%s"`, a.DefaultValue)
						break
					default:
						qq.Args[i].DefaultValue = a.DefaultValue
					}
				}
			}
		}

		schemas = append(schemas, qq)
	}

	return schemas
}

func buildTypes(tys []schema.Types) []Types {
	var result []Types

	for _, v := range tys {
		tt := Types{
			Name:        v.Name,
			Description: convertTitle(v.Description),
		}

		tt.Fields = make([]Field, len(v.Fields))
		for i, f := range v.Fields {
			tt.Fields[i].Name = f.Name
			tt.Fields[i].Description = f.Description
			tt.Fields[i].Type = buildType(f.Value, f.Type.IsNotNull, f.Type.IsArray, f.Type.IsNotNullArray)
			tt.Fields[i].TypeGo = buildTypeForGo(f.Name, f.Value, f.Type.IsArray)
		}

		result = append(result, tt)
	}

	return result
}

func buildInputs(tys []schema.Input) []Inputs {
	var result []Inputs

	for _, v := range tys {
		ii := Inputs{
			Name:        v.Name,
			Description: convertTitle(v.Description),
		}

		ii.Fields = make([]Arg, len(v.Fields))
		for i, f := range v.Fields {
			ii.Fields[i].Name = f.Name
			ii.Fields[i].Description = f.Description
			ii.Fields[i].Type = buildType(f.Value, f.Type.IsNotNull, f.Type.IsArray, f.Type.IsNotNullArray)
			if f.DefaultValue != nil {
				switch fmt.Sprintf("%T", f.DefaultValue) {
				case "string":
					ii.Fields[i].DefaultValue = fmt.Sprintf(`"%s"`, f.DefaultValue)
					break
				default:
					ii.Fields[i].DefaultValue = f.DefaultValue
				}
			}
		}

		result = append(result, ii)
	}

	return result
}

func buildScalar(tys []schema.Scalar) []Scalar {
	var result []Scalar

	for _, v := range tys {
		in := Scalar{
			Name:        v.Name,
			Description: convertTitle(v.Description),
			Struct:      `Value string`,
			Func: `String() string {
	return scalar.Value
}`,
			FuncNew: fmt.Sprintf(`(v string) *%sScalar {
	return &%sScalar{Value: v}
}`, v.Name, v.Name),
			FuncSerialize: fmt.Sprintf(`
		switch value := value.(type) {
		case %sScalar:
			return value.String()
		case *%sScalar:
			v := *value
			return v.String()
		default:
			return nil
		}`, v.Name, v.Name),
			FuncParseValue: fmt.Sprintf(`
		switch value := value.(type) {
		case %sScalar:
			return value.String()
		case *%sScalar:
			v := *value
			return v.String()
		default:
			return nil
		}`, v.Name, v.Name),
			FuncParseLiteral: fmt.Sprintf(`
		switch valueAST := valueAST.(type) {
		case *ast.StringValue:
			return New%sScalar(valueAST.Value)
		default:
			return nil
		}`, v.Name),
		}

		result = append(result, in)
	}

	return result
}

func buildType(name string, isNotNull, isArray, isNotNullArray bool) string {
	t := ""
	switch strings.ToLower(name) {
	case "string":
		t = "graphql.String"
		break
	case "int":
		t = "graphql.Int"
		break
	case "float":
		t = "graphql.Float"
		break
	case "boolean":
		t = "graphql.Boolean"
		break
	default:
		t = fmt.Sprintf("%sType", strings.Replace(name, "*", "", -1))
		break
	}

	if isNotNull {
		t = fmt.Sprintf("graphql.NewNonNull(%s)", t)
	}

	if isArray {
		t = fmt.Sprintf("graphql.NewList(%s)", t)
	}

	if isNotNullArray {
		t = fmt.Sprintf("graphql.NewNonNull(%s)", t)
	}

	return t
}

func buildTypeForGo(name, typeField string, isArray bool) string {
	parseName := func(s string) string {
		r := ""
		for _, v := range strings.Split(s, "_") {
			r += convertTitle(string(v))
		}

		return strings.Replace(r, "Id", "ID", -1)
	}

	typeGo := "%s"
	if isArray {
		typeGo = "[]" + typeGo
	}

	t := fmt.Sprintf("%s %s `json:\"%s\"`", parseName(name), typeGo, name)
	switch strings.ToLower(typeField) {
	case "string":
		t = fmt.Sprintf(t, "string")
		break
	case "int":
		t = fmt.Sprintf(t, "int")
		break
	case "float":
		t = fmt.Sprintf(t, "float64")
		break
	case "boolean":
		t = fmt.Sprintf(t, "bool")
		break
	default:
		if strings.Contains(typeField, "Enum") {
			t = fmt.Sprintf(t, "string")
		} else if strings.Contains(typeField, "Scalar") {
			t = fmt.Sprintf(t, "*"+typeField)
		} else {
			t = fmt.Sprintf(t, typeField)
		}
		break
	}

	return t
}
