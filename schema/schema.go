package schema

import (
	"fmt"
	"regexp"
	"strconv"
)

// Type structure for restrictions of a kind
type Type struct {
	IsArray        bool
	IsNotNullArray bool
	IsNotNull      bool
}

// Arg structure for queries and mutations arguments
type Arg struct {
	Name         string
	Description  string
	DefaultValue interface{}
	Value        string
	Type
}

// Fragment structure for queries and mutations
type Fragment struct {
	Name        string
	Description string
	Args        []Arg
	Result      string
	Type
}

// Schema structure of the graphql elements
type Schema struct {
	TypeQuery    string
	TypeMutation string
	Query        []Fragment
	Mutation     []Fragment
	Enums        []Enum
	Scalars      []Scalar
	Inputs       []Input
	Types        []Types
}

// Restriction structure of the graphql restruction
type Restriction struct {
	Value string
	Type
}

// GetSchema get all the elements of the .graphql document
func (s *Schema) GetSchema(data string) *Schema {
	schema := ParseRegex(`schema[\t ]*{([\t ]*(\n[\t ]*[\w: !\[\](),=#]*)*)[\t ]*}`, data)
	if len(schema) > 0 {
		if query := ParseRegex(`[\t ]*query[\t ]*:[\t ]*([\w_.]+)`, schema[0][0]); len(query) > 0 {
			s.TypeQuery = query[0][0]
		}
		if mutation := ParseRegex(`[\t ]*mutation[\t ]*:[\t ]*([\w_.]+)`, schema[0][0]); len(mutation) > 0 {
			s.TypeMutation = mutation[0][0]
		}
	}

	s.GetEnums(data)
	s.GetScalars(data)
	s.GetInputs(data)
	if s.TypeQuery != "" {
		s.Query = s.GetFragment(s.TypeQuery, data)
	}
	if s.TypeMutation != "" {
		s.Mutation = s.GetFragment(s.TypeMutation, data)
	}
	s.GetTypes(data)
	return s
}

// GetFragment get the queries and mutations
func (s *Schema) GetFragment(name, data string) []Fragment {
	var result []Fragment

	data = ParseRegex(fmt.Sprintf(`(?s)(?m)type[\t ]*%s[\t ]*{(.*?)}`, name), data)[0][0]
	fragments := ParseRegex(`([\w]+)[\t ]*(\((.*)\))?[\t ]*:[\t ]*(.*)`, data)

	for k, comment := range GetComments(data, "") {
		fragment := fragments[k]
		args := ParseRegex(`([\w]+)[\t ]*:[\t ]*([^,= ]*)[\t ]*=?[\t ]*[\t ]*([\w"_\d ]*)`, fragment[2])
		f := Fragment{
			Description: comment,
			Name:        fragment[0],
			Args:        make([]Arg, len(args)),
		}

		for index, arg := range args {
			typeArg := GetRestrictions(arg[1])
			f.Args[index] = Arg{
				Name:         arg[0],
				Value:        s.CheckTypes(typeArg.Value),
				DefaultValue: ParseDefaultValue(arg[2]),
				Type:         typeArg.Type,
			}
		}

		resp := GetRestrictions(fragment[3])
		f.Result = resp.Value
		f.Type = resp.Type

		result = append(result, f)
	}

	return result
}

// ParseRegex extract the values from a regular expression
func ParseRegex(regex, data string) [][]string {
	r := regexp.MustCompile(regex)
	match := r.FindAllString(data, -1)

	find := make([][]string, len(match))
	for k, v := range match {
		find[k] = r.FindStringSubmatch(v)[1:]
	}

	return find
}

// GetComments get comments of document
func GetComments(data, item string) []string {
	var comments []string
	for _, v := range ParseRegex(fmt.Sprintf(`\#[\t ]*(.*)\n[\t ]*%s`, item), data) {
		comments = append(comments, v[0])
	}

	return comments
}

// GetRestrictions get retractions of an item
func GetRestrictions(item string) Restriction {
	restrictions := ParseRegex(`[\[{ ]*([\w._\-*]*)(!\])?(\])?(!)?`, item)[0]
	return Restriction{
		Value: restrictions[0],
		Type: Type{
			IsArray:        restrictions[1] != "" || restrictions[2] != "",
			IsNotNull:      restrictions[3] != "",
			IsNotNullArray: restrictions[1] != "",
		},
	}
}

// CheckTypes add the data type to the value to refer to the variable name
func (s *Schema) CheckTypes(value string) string {
	for _, en := range s.Enums {
		if en.Name == value {
			return value + "Enum"
		}
	}
	for _, in := range s.Inputs {
		if in.Name == value {
			return value + "Input"
		}
	}
	for _, sc := range s.Scalars {
		if sc.Name == value {
			return value + "Scalar"
		}
	}

	return value
}

// ParseDefaultValue transform a string to interface with the value
func ParseDefaultValue(value string) interface{} {
	var defaultValue interface{}
	var err error
	if value != "" {
		defaultValue, err = strconv.ParseBool(value)
		if err != nil {
			defaultValue, err = strconv.ParseFloat(value, 64)
			if err != nil {
				defaultValue = value
			}
		}
	}

	return defaultValue
}
