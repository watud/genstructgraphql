package schema

// Input structure for graphql input
type Input struct {
	Name        string
	Description string
	Fields      []Arg
}

// GetInputs get all the inputs of the document
func (s *Schema) GetInputs(data string) {
	inputs := ParseRegex(`(?s)(?m)input[\t ]*([A-Z][\w_]*)[\t ]*{(.*?)}`, data)

	for k, comment := range GetComments(data, "input ") {
		input := inputs[k]
		ii := Input{
			Name:        input[0],
			Description: comment,
		}

		fields := ParseRegex(`\#[\t ]*(.*)\n[\t ]*(.*)[\t ]*:[\t ]*([^ ]*)([\t ]*=[\t ]*(.*))?`, input[1])
		for _, v := range fields {
			typeArg := GetRestrictions(v[2])

			ii.Fields = append(ii.Fields, Arg{
				Description:  v[0],
				Name:         v[1],
				Value:        s.CheckTypes(typeArg.Value),
				DefaultValue: ParseDefaultValue(v[4]),
				Type:         typeArg.Type,
			})
		}

		s.Inputs = append(s.Inputs, ii)
	}
}
