package schema

// Scalar structure for graphql scalar
type Scalar struct {
	Name        string
	Description string
}

// GetScalars get all the scalars of the document
func (s *Schema) GetScalars(data string) {
	scalars := ParseRegex(`scalar[\t ]*([A-Z].*)`, data)

	for k, comment := range GetComments(data, "scalar ") {
		s.Scalars = append(s.Scalars, Scalar{
			Name:        scalars[k][0],
			Description: comment,
		})
	}
}
