package schema

import (
	"strings"
)

// Types structure of the graphql type
type Types struct {
	Name        string
	Description string
	Fields      []Arg
}

// FieldEnum structure of the graphql enums values
type FieldEnum struct {
	Name  string
	Value string
}

// Enum structure of the graphql enum
type Enum struct {
	Name        string
	Description string
	Values      []FieldEnum
}

// GetEnums get all the enums of the document
func (s *Schema) GetEnums(data string) {
	enums := ParseRegex(`(?s)(?m)enum[\t ]*([A-Z][\w_]*)[\t ]*{(.*?)}`, data)

	for k, comment := range GetComments(data, "enum ") {
		enum := enums[k]
		ee := Enum{
			Name:        enum[0],
			Description: comment,
		}

		for _, v := range ParseRegex(`(\w*)([\t ]*=[\t ]*(.*))?`, enum[1]) {
			f := FieldEnum{
				Name:  strings.Trim(v[0], " "),
				Value: strings.Trim(v[2], " "),
			}
			if len(f.Name) > 0 {
				if f.Value == "" {
					f.Value = f.Name
				}
				ee.Values = append(ee.Values, f)
			}
		}

		s.Enums = append(s.Enums, ee)
	}
}

// GetTypes get all the types of the document
func (s *Schema) GetTypes(data string) {
	types := ParseRegex(`(?s)(?m)type[\t ]*([A-Z].*?)[\t ]*{(.*?)}`, data)

	for k, comment := range GetComments(data, "type ") {
		ty := types[k]
		if ty[0] != s.TypeQuery && ty[0] != s.TypeMutation {
			tt := Types{
				Name:        ty[0],
				Description: comment,
			}

			fields := ParseRegex(`\#[\t ]*(.*)\n[\t ]*(.*)[\t ]*:[\t ]*(.*)`, ty[1])
			for _, v := range fields {
				typeArg := GetRestrictions(v[2])

				tt.Fields = append(tt.Fields, Arg{
					Description: v[0],
					Name:        v[1],
					Value:       s.CheckTypes(typeArg.Value),
					Type:        typeArg.Type,
				})
			}

			s.Types = append(s.Types, tt)
		}
	}
}
